
#!/usr/bin/env bash

#Set default locales
echo -e 'LC_ALL=en_US.UTF-8\nLANG=en_US.UTF-8' >> /etc/environment

#Create a profile file if not exists
su - vagrant -c 'touch ~/.bash_profile'

#Install nvm - Node Version Manager
su - vagrant -c 'curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.26.1/install.sh | bash'

#Install node with nvm
su - vagrant -c 'nvm install 0.12.7'

#Select default node version at the start
su - vagrant -c "echo -e '\nnvm use 0.12.7' >> ~/.bash_profile"